var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
  res.status(200);
  res.json({message: 'Hello World!'});
});

router.get('/stats', function (req, res) {
  res.status(200);
  res.json({
    user_count: req.app.locals.user_count,
    job_count: req.app.locals.job_count,
    job_number: req.app.locals.job_number
  });
});

module.exports = router;
