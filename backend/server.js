// express.js server
var express = require('express');
var app = express();

// load the wanted module
// TODO: dynamic loading
var task = require('./../modules/' + process.env.MODULE_NAME + '/Server.js');

// base http server
var server = require('http').Server(app);

// require socket.io and bind to http server
var io = require('socket.io')(server);

// express.js middleware
// app.use(require('morgan')('combined')); // logger
app.use(require('compression')()); // compression
app.use(require('helmet')()); // security headers

// serve ../frontend folder as static files
// deny dotfiles for better security
app.use('/', express.static(__dirname + '/../frontend/', {dotfiles: 'deny'}));
app.use('/module/', express.static(__dirname + '/../modules/' + process.env.MODULE_NAME));

// use external file for API routes
app.use('/api/', require('./api'));

//TODO: new file _________________________________________________________________________________

// Array of all jobs
var todo = [];

// function for getting a new job
function getJob(socket) {
  console.log(socket.id + ' needs a new job');

  // empty job object
  var job;

  // check if there are any jobs on the todo list
  if (todo.length <= 0) {
    // if not request a new job
    job = task.generateJob();
  } else {
    // get the last object of the todo list and delete it
    job = todo[todo.length - 1];
    todo.pop();
  }

  // check if returned value is null
  if (job != null) {
    socket.job = job;
    socket.emit('job', job);
    updateJobNumber();
  } else {
    task.onAllResults();

    /*// look for active jobs which might still active
    var activeJobs = (getActiveJobCount() > 0) ? true : false;
    if (todo.length <= 0 && !activeJobs) {
      // everything is done
      task.onAllResults();
    }*/
  }
  updateActiveJobCount();
}

// count all active jobs
function getActiveJobCount() {
  var job_count = 0;
  for (var socket in io.sockets.sockets) {
    if (io.sockets.sockets[socket].job) {
      job_count++;
    }
  }
  return job_count;
}

function getUserCount() {
  var user_count = 0;
  for (var socket in io.sockets.sockets) {
    if (io.sockets.sockets[socket].role === 'worker') {
      user_count++;
    }
  }
  return user_count;
}

function updateActiveJobCount() {
  app.locals.job_count = getActiveJobCount();
  publishStats();
}

function updateUserCount() {
  app.locals.user_count = getUserCount();
  publishStats();
}

function updateJobNumber() {
  if (app.locals.job_number === null) {
    app.locals.job_number++;
  } else {
    app.locals.job_number = 0;
  }
  publishStats();
}

function publishStats() {
  // send all stats in stats channel
  io.sockets.emit('stats', {
    user_count: app.locals.user_count,
    job_count: app.locals.job_count,
    job_number: app.locals.job_number
  });
}

// socket.io functions
io.on('connection', function (socket) {
  // notify everybody about connection
  console.log(socket.id + ' connected');
  updateUserCount();

  socket.on('disconnect', function () {
    // update everything for others on disconnect
    console.log(socket.id + ' disconnected');
    updateUserCount();

    // push job without owner into todo if there is one
    if (socket.job) {
      todo.push(socket.job);
    }
  });

  socket.on('role', function (role) {
    socket.role = role;
    if (socket.role === 'worker') {
      // send a job to a user on connection
      getJob(socket);
    }
  });

  // called when a worker notifies about a job result
  socket.on('job', function (job) {
    // check if job was successful
    if (job.success) {
      // tell the task the result
      task.onResult(job.result);

      // log the result with matching parameter
      console.log(String(socket.job) + " resulted in " + String(job.result));

      socket.job = false;

      // request a new hob for this socket
      getJob(socket)
    } else {
      // job failed, display error
      console.error(job.error);
    }
  });
});

// bind server to PORT in environment or 8000
const port = process.env.PORT || 8000;
server.listen(port);

// start a control interface via telnet if required by environment
if (process.env.TELNET_PORT) {
  io.use(require('monitor.io')({port: process.env.TELNET_PORT}));
}
