var user_count = new TimeSeries();
var job_count = new TimeSeries();

var smoothie = new SmoothieChart({millisPerPixel:53,grid:{fillStyle:'#ffffff',strokeStyle:'#434343'},labels:{fillStyle:'#434343'}});

smoothie.addTimeSeries(user_count, {lineWidth:5,strokeStyle:'#00a6e2'});
smoothie.addTimeSeries(job_count, {lineWidth:5,strokeStyle:'#00a6e2'});

smoothie.streamTo(document.getElementById('chart'));

var socket = io();

socket.on('connect', function () {
  socket.emit('role', 'stats');
});

socket.on('stats', function(stats) {
  document.getElementById('user_count').innerText = stats.user_count;
  document.getElementById('job_count').innerText = stats.job_count;
  
  user_count.append(new Date().getTime(), stats.user_count);
  job_count.append(new Date().getTime(), stats.job_count);
});