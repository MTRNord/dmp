/**
 * Created by jaro on 11.06.16.
 * this is the Worker JS file...
 */

function start() {
    dOut("started Worker...");
    //start the socket.io connection...
    var socket = io();

    socket.on('connect', function () {
        socket.emit('role', 'worker');
    });

    socket.on('job', function (job) {
        dOut("got job: " + job);
        var result = null;
        try {
            result = {success: true, result: dmp(job)};
        } catch (e) {
            result = {success: false, error: e};
            console.error(e)
        }
        dOut("finished job with result: " + JSON.stringify(result));
        socket.emit('job', result);
    });
}

function require(module) {//this function is provided to the module, so it can import libs TODO: test me!
    document.head.innerHTML = "<script type='application/javascript' language='JavaScript' async='true' src='/module/" + module + "'></script>\n" + document.head.innerHTML;
}

function dOut(msg) {//this is the internal log function
    if (false) {
        console.log(msg);
        document.getElementById("dOut").innerHTML += msg + "<br>";
    }
}

window.onload = start;
